﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using System.Collections;
using Microsoft.Win32;

namespace PhotoSorter
{
    internal class FileWalker
    {
        public FileWalker() //string dir, string ext = "")
        {

        }
        public string TargetDir { get; set; }
        public string SearchPattern { get; set; }
        public bool SlideShow { get; set; }
        public int Wait { get; set; }
        static Process proc;
        static object _lock = new object();
        // Process all files in the directory passed in, recurse on any directories 
        // that are found, and process the files they contain.
        public void ProcessDirectory(Action<string,bool,int > ProcessFile)// string targetDirectory)
        {
            // Process the list of files found in the directory.
            Console.WriteLine("{0}, {1}", TargetDir, SearchPattern);
            var ext = SearchPattern.Split(',');
            var cmd = GetRelatedCommand(ext);
            foreach (var e in ext)
            {
                string[] fileEntries = Directory.GetFiles(TargetDir, e, SearchOption.TopDirectoryOnly);
                if (fileEntries.Count() < 1 )
                {
                    Console.WriteLine("Processing Directory - {0} - Has no files matching {1}", TargetDir, e);
                }
                foreach (string fileName in fileEntries)
                {
                    //if (!
                    ProcessFile(fileName, SlideShow, Math.Max(Wait, 5));
                    //)
                    //  {
                    //     break;
                    // }
                }
            }
        }
        private string GetRelatedCommand (string[] ext)
        {
            return "";
        }
        public static void ProcessFile(string fname, bool ss, int inWait = 20)
        {
            if (!File.Exists(fname))
            {
                throw new ArgumentException("Filename doesn't exist");
            }
            ProcessStartInfo psi = new ProcessStartInfo();
            psi.UseShellExecute = false;
            psi.FileName = @"C:\Program Files (x86)\BraveSoftware\Brave-Browser\Application\brave.exe";//TODO: make this runtime configurable
            //@"c:\Program Files (x86)\Google\Chrome\Application\chrome.exe";// "start";
            psi.Arguments = string.Format("{0} --enable-kiosk-mode --incognito --new-window", fname); // string.Format("\"ms-photos:\" {0}", fname);
                                                                                                      //"shell: appsFolder\Microsoft.Windows.Photos_2022.30060.30007.0_x64__8wekyb3d8bbwe\Microsoft.Photos.exe";
            int shortPauseMS = 4000;
            proc = Process.Start(psi);
            if (proc != null)
                Console.WriteLine("Process created: {0} -- {1}", proc.Id, proc.ProcessName);
            if (ss)
            {
                lock (_lock)
                {/* a timer AND a sleep... Not sure what I was thinking... if this is a screen saver.. I want this to be synchronus

                    var t = new System.Timers.Timer(inWait * 1000);
                    t.Elapsed += T_Elapsed;
                    t.AutoReset = false;
                    t.Start();
                    */
                    Thread.Sleep(shortPauseMS);
                    
                }
            }
            try
            {
                if (!ss || !Console.NumberLock)  // Not a slide show, or if it is, the numlock is up.. so pause
                {
                    Console.WriteLine("Keep File? ({0}) y/n", fname);
                    var ans = Console.ReadKey();
                    if (ans.KeyChar == 'n')
                    {
                        Move(fname, "ToBeDeleted");
                        return;
                    }
                    Console.WriteLine("Group or Rank? (g or number [1-9]");
                    ans = Console.ReadKey();
                    if (ans.KeyChar == 'g')
                    {
                        Console.WriteLine("Group name:");
                        var subDir = Console.ReadLine();
                        Move(fname, subDir);
                    }
                    else
                    {
                        int rank = -1;
                        if (int.TryParse(ans.KeyChar.ToString(), out rank))
                        {
                            Move(fname, string.Format("keep-{0}", rank));
                        }
                    }
                    Console.WriteLine("Quit (q or anything else to continue)");
                    ans = Console.ReadKey();
                    if (ans.KeyChar == 'q')
                    {
                        Environment.Exit(0);
//                        throw new ArgumentException("This shouldn't be an exception, we just need to quit properly.");
                       
                    }
                } else //if ()
                {
                    Console.WriteLine("Enjoy Photo {0} for about ~{1} seconds", fname, inWait);
                    if (Console.CapsLock && Console.NumberLock)
                    {
                        Console.WriteLine("If you don't pop Capslock soon I'll quit.");
                    }
                    Thread.Sleep((inWait ) * 1000- shortPauseMS);
                    //TODO give me one more chance to deal with pic?
                    if (Console.CapsLock  && Console.NumberLock)
                    {
                        Console.WriteLine("CapsLock and Numlock engaged, time to go.");
                        Environment.Exit(0);
                    }

                }
            }
            catch(ArgumentException ae)
            {
                throw ae;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
            }
            finally
            {
                KillProcess();
            }
            
        }
        private static void KillProcess()
        {
            try
            {
                if (proc != null)
                {
                    Console.WriteLine("Killing process {0} {1}", FileWalker.proc.Id, FileWalker.proc.ProcessName);
                    FileWalker.proc.Kill();
                }
                else
                {
                    Console.WriteLine("Process is null");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("TimerException: {0}", ex);
            }
        }
        private static void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            lock (_lock)
            {
                if (Console.NumberLock)
                {
                    KillProcess();
                }
                // Dispose of the timer here:
                ((System.Timers.Timer)sender).Dispose();
            }
        }

        private static void Move(string fname, string nuSubdir)
        {
            string justFile = Path.GetFileName(fname);
            string justDir = Path.GetDirectoryName(fname);
            //Path.GetDirectoryName"..", 
            string nuDir = Path.Combine(justDir, nuSubdir);
            //THIS Isn't needed because documentation for CreateDirectory states it won't create if already made if (!Directory.Exists(nuDir))            { }
            Directory.CreateDirectory(nuDir);
           
            string movedFile = Path.Combine(nuDir, justFile);
            File.Move(fname, movedFile);
        }
    }
}
