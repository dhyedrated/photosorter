﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CommandLine;

namespace PhotoSorter
{
    class Program
    {
        static void Main(string[] args)
        {
            // CommandLine.Text.HelpText ht = new CommandLine.Text.HelpText()
         /*   var ps2 = new ParserSettings()
            {
                AutoHelp = true,
                AutoVersion = true,
                CaseSensitive = false,
                EnableDashDash = true
            };*/
            var p = new Parser((ParserSettings ps) => {
                  ps=  new ParserSettings()
                    {
                        AutoHelp = true,
                        AutoVersion = true,
                        CaseSensitive = false,
                        EnableDashDash = true
                    };
                }
            );// new Action<ParserSettings>config);
            var pr = p.ParseArguments<Options>(args).WithParsed(RunOptions);
            CommandLine.Text.HelpText ht = new CommandLine.Text.HelpText();
            ht.AddOptions<Options>(pr);
            ht.AddDashesToOption = true;
            ht.AutoHelp = true;
            Console.Write(CommandLine.Text.HelpText.RenderUsageText<Options>(pr));

             //    .WithNotParsed(HandleParseError);
            /*  var res =  CommandLine.Parser.Default.ParseArguments<Options>(args)
                  .WithParsed(RunOptions)
                   .WithNotParsed(HandleParseError);
              if(res.)*/
        }
    static void RunOptions(Options opts)
    {
        //handle options
        if (string.IsNullOrWhiteSpace(opts.Directory))
        {
            opts.Directory = Environment.CurrentDirectory;
        }
        var fw = new FileWalker()
        {
            TargetDir = opts.Directory,
            SearchPattern = opts.Extensions,
            SlideShow = opts.Slideshow,
            Wait = opts.Wait
        };
        fw.ProcessDirectory(FileWalker.ProcessFile);

    }
    static void HandleParseError(IEnumerable<Error> errs)
    {
        //handle errors
        Console.WriteLine("Error parsing arguments {0}", errs);
    }
}
}
