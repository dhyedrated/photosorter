﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace PhotoSorter
{
    class Options
    {
        [Option('d', "directory", Required = false,   HelpText = "Directory to go through, otherwise current directory")]
        public string Directory { get; set; }

        [Option('v', "verbose", Default = true,  HelpText = "Prints all messages to standard output.")]
        public bool Verbose { get; set; }

        [Option('e', "ext", Default = "*.jpg", HelpText = "Comma seperated list of extension to include")]
        public string Extensions { get; set; }

        [Option('s', "slideshow", Default = false, HelpText = "Opens images for <wait> seconds(def:20), then closes them")]
        public bool Slideshow { get; set; }

        [Option('w', "wait", Default = 20, HelpText = "Time to wait between images during slideshow")]
        public int Wait { get; set; }
        /*  examples from another fork 
         *  [ParserState]
          public IParserState LastParserState { get; set; }
          */
        //  [HelpOption]  -d C:\inbox\bkupdec2011\photos\pic
        //[]
        public string GetUsage()
        {
            return new HelpText("A tool for me", "11july022");
                //.AutoBuild<string>(//.AutoBuild(this,               (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
        //*/
    }
}
